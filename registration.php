<?php
/**
 * Copyright 2016 notifyapp.io. All rights reserved.
 * See COPYING.txt for license details.
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'NotifyApp_Notify',
    __DIR__
);
