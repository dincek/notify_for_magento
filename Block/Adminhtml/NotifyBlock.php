<?php
namespace NotifyApp\Notify\Block\Adminhtml;

use Magento\Backend\Block\Template;

class NotifyBlock extends Template
{
    private $store_url;

    private $store_id;

    private $notify_url;

    private $time;

    private $key;

    private $message;

    public function setStoreUrl($store_url)
    {
        $this->store_url = $store_url;
    }

    public function setStoreId($store_id)
    {
        $this->store_id = $store_id;
    }

    public function setNotifyUrl($notify_url)
    {
        $this->notify_url = $notify_url;
    }

    public function setTime($time)
    {
        $this->time = $time;
    }

    public function setKey($key)
    {
        $this->key = $key;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getStoreUrl()
    {
        return $this->store_url;
    }

    public function getStoreId()
    {
        return $this->store_id;
    }

    public function getNotifyUrl()
    {
        return $this->notify_url;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getMessage()
    {
        return $this->message;
    }
}
