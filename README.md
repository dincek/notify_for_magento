#MANUAL INSTALLATION
Run these commands from magento root directory.

##Install module:

Go to your Magento web folder and go to folder /app/code/ (it is possible that code folder does not exist, create it)
Unzip content of App_Notify.zip to /app/code/
Fix permissions for that folder, check that apache / you web browser has access to that files, usualy you can do this:
chown apache:apache -R /app/code/

Run commands:

`php bin/magento module:enable NotifyApp_Notify`

`php bin/magento setup:upgrade`

`php bin/magento cache:flush`

(optional) `chown apache:apache -R *`

##Uninstall module:

Run commands:

`php bin/magento module:disable NotifyApp_Notify`

`php bin/magento setup:upgrade`

`php bin/magento cache:flush`

`rm -rf app/code/NotifyApp/`

(optional) `chown apache:apache -R *`


#COMPOSER INSTALLATION
Run these commands from magento root directory.

##Install Composer
First, check if Composer is already installed:

In a command prompt, enter any of the following commands:

* composer --help
* composer list --help


If command help displays, Composer is already installed.

If an error displays, use the following steps to install Composer.

To install Composer:

Change to or create an empty directory on your Magento server.

Enter the following commands:

`curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer`

##Install module:

Run commands:

`composer config repositories.notifyapp vcs <repo>`

`composer require notifyapp/notify:dev-master`

(optional) If user never before used composer for installation of components / modules for Magento, he needs to register at https://www.magentocommerce.com/magento-connect/customerdata/accessKeys/list/ and use his public key as username and private key as password for repo.magento.com when asked by Composer system.

`php bin/magento module:enable NotifyApp_Notify`

`php bin/magento setup:upgrade`

`php bin/magento cache:flush`

(optional) `chown apache:apache -R *`

##Update module:

Run commands:

`composer update`

`php bin/magento setup:upgrade`

`php bin/magento cache:flush`

(optional) `chown apache:apache -R *`

##Uninstall module:

Run commands:

`php bin/magento module:uninstall --remove-data NotifyApp_Notify`

`php bin/magento setup:upgrade`

`php bin/magento cache:flush`

(optional) `chown apache:apache -R *`
