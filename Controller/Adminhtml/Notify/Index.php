<?php

namespace NotifyApp\Notify\Controller\Adminhtml\Notify;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use NotifyApp\Notify\Observer\Notify;

class Index extends \Magento\Backend\App\Action
{

    /**
     * @var PageFactory
     */
    private $resultPageFactory;
    /**
     * @var scopeConfig
     * Needed to retrieve config values
     */
    private $scopeConfig;

    /**
     * @var \NotifyApp\Notify\Model\ResourceModel\NotifyKey\CollectionFactory
     */
    private $notifyKeyCollectionFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ScopeConfigInterface $scopeConfig,
        \NotifyApp\Notify\Model\ResourceModel\NotifyKey\CollectionFactory $notifyKeyCollectionFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig; // Needed to retrieve config values
        $this->notifyKeyCollectionFactory = $notifyKeyCollectionFactory;
    }

    public function _getStore()
    {
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore($storeId);
    }

    /**
     * Index Action
     */
    public function execute()
    {
        $notifyKeys = $this->notifyKeyCollectionFactory
            ->create()
            ->addFilter('id', 1);
        $key = '';
        foreach ($notifyKeys as $obj) {
            $key = $obj->getKey();
        }

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('App_Notify::dashboard');
        $resultPage->getConfig()->getTitle()->prepend(__('Notify')); // Changing the page title
        $store = $this->_getStore();
        if ($store->getId() == 0) {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
            $resultRedirect->setPath("notify/notify/index/store/" . $store->getId());
            return $resultRedirect;
        }
        $resultPage->getLayout()->getBlock('Notify')->setStoreId($store->getId());
        $resultPage->getLayout()->getBlock('Notify')->setStoreUrl($store->getBaseUrl());
        // check installation with Notify server
        if (Notify::notifyCheck($store->getBaseUrl(), $key)['success']) {
            $resultPage->getLayout()->getBlock('Notify')->setNotifyUrl(Notify::$DOMAIN);
            $resultPage->getLayout()->getBlock('Notify')->setKey($key);
            $resultPage->getLayout()->getBlock('Notify')->setTime(time());
        } else {
            // re-init
            Notify::notifyInit($store->getBaseUrl());
            // show message
            $resultPage->getLayout()->getBlock('Notify')
                ->setMessage("Reinitialization for shop URL is required. Refresh this page in few seconds.");
        }
        return $resultPage;
    }
}
