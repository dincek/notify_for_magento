<?php

/**
 * Copyright 2016 notifyapp.io. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace NotifyApp\Notify\Api;

/**
 * Interface AuthorizationInterface
 * @package App\Notify\Api
 */
interface AuthorizationInterface
{
    /**
     * Authorize with Notify
     *
     * @api
     * @param string $payload Base64 encoded JSON request
     * @return string Authorization response
     */
    public function auth($payload);

    /**
     * Get recent orders
     *
     * @param string $payload Base64 encoded JSON request
     * @return string List of orders
     */
    public function orders($payload);
}
