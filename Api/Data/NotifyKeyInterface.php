<?php

namespace NotifyApp\Notify\Api\Data;

interface NotifyKeyInterface
{
    const ID = 'id';
    const KEY = 'key';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get Key
     *
     * @return string
     */
    public function getKey();

    /**
     * Set ID
     *
     * @param int $id
     * @return \NotifyApp\Notify\Api\Data\NotifyKeyInterface
     */
    public function setId($id);

    /**
     * Set Key
     *
     * @param string $key
     * @return \NotifyApp\Notify\Api\Data\NotifyKeyInterface
     */
    public function setUrlKey($key);
}
