<?php

/**
 * Copyright 2016 notifyapp.io. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace NotifyApp\Notify\Model;

use Crypt_RSA;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Model\ScopeInterface;
use NotifyApp\Notify\Api\AuthorizationInterface;
use NotifyApp\Notify\Observer\Notify;

class Authorization implements AuthorizationInterface
{
    private $scopeConfig;

    /** @var OrderRepositoryInterface */
    private $orderRepository;

    /** @var SearchCriteriaBuilder */
    private $searchCriteriaBuilder;

    /** @var ProductRepository */
    private $productRepository;

    /** @var FilterBuilder */
    private $filterBuilder;

    /** @var \NotifyApp\Notify\Model\ResourceModel\NotifyKey\CollectionFactory */
    private $notifyKeyCollectionFactory;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ProductRepository $productRepository,
        FilterBuilder $filterBuilder,
        \NotifyApp\Notify\Model\ResourceModel\NotifyKey\CollectionFactory $notifyKeyCollectionFactory
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productRepository = $productRepository;
        $this->filterBuilder = $filterBuilder;
        $this->notifyKeyCollectionFactory = $notifyKeyCollectionFactory;
    }

    public static $public_key = "-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAz4O2MJFawlvLfytHUMLc
TZ4qw8UglP/p8SZy0+3GUBqeKbY7cwihF6KZA9BKmw0hyG/IhLnY4xS4G5gQgL3g
Se2b6muEx0x+jt+XKYVtpnuWy0ysuuHAYExyI8PWQIVYnVA8wjVxZMdOyPtQQnQV
aCL/ppCWDyAP8hg8eQ5y1xA2B0KGdieYnjJWpQ+6ZlgrtKs35jo4Y2zfDhCAFqVs
HfFs6j2eu3XAjC1DfEb42R6XzFIgVf14Ug/kpFW5Sd76iKhXjpoSWAvEhC2Ze1mO
bhMjSvgpF7/xTFh8R3OoI0qndtc/u8Q94RkLtZ+XQYi008pG0hKzNO64+5H3QE5O
RwIDAQAB
-----END PUBLIC KEY-----";

    /**
     * Authorize with Notify
     *
     * @api
     * @param string $payload Base64 encoded JSON request
     * @return string Authorization response
     */
    public function auth($payload)
    {
        $data = json_decode(base64_decode($payload), true);
        if (count($data) == 0) {
            return $data;
        }
        $notifyKeys = $this->notifyKeyCollectionFactory
            ->create()
            ->addFilter('id', 1);
        $key = '';
        foreach ($notifyKeys as $obj) {
            $key = $obj->getKey();
        }

        // encrypt
        include_once('phpseclib/Crypt/RSA.php');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $stores = [];
        foreach ($storeManager->getStores() as $store) {
            $stores[] = ['id' => $store->getId(), 'url' => $store->getBaseUrl()];
        }
        $email = $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE);
        $name = $this->scopeConfig->getValue('trans_email/ident_general/name', ScopeInterface::SCOPE_STORE);
        $sys_email = $this->scopeConfig->getValue('trans_email/ident_sysadmin/email', ScopeInterface::SCOPE_STORE);
        $sys_name = $this->scopeConfig->getValue('trans_email/ident_sysadmin/name', ScopeInterface::SCOPE_STORE);
        $sales_email = $this->scopeConfig->getValue('trans_email/ident_sales/email', ScopeInterface::SCOPE_STORE);
        $sales_name = $this->scopeConfig->getValue('trans_email/ident_sales/name', ScopeInterface::SCOPE_STORE);
        $settings = json_encode([
            'key' => $key,
            'stores' => $stores,
            'general' => ['email' => $email, 'name' => $name],
            'sysadmin' => ['name' => $sys_name, 'email' => $sys_email],
            'sales' => ['name' => $sales_name, 'email' => $sales_email]
        ]);
        $rsa_magento = $objectManager->create('Crypt_RSA');
        $rsa_magento->loadKey(self::$public_key);
        $encrypted_payload = base64_encode($rsa_magento->encrypt($settings));
        $timestamp = isset($data['request']['timestamp']) ? $data['request']['timestamp'] : '';
        $signature = isset($data['request']['signature']) ? $data['request']['signature'] : '';

        if ($rsa_magento->verify($timestamp, base64_decode($signature))) {
            return [[
                "timestamp" => $timestamp,
                "signature" => $signature,
                "encrypted_payload" => $encrypted_payload
            ]];
        } else {
            return [[
                "timestamp" => $timestamp,
                "signature" => $signature,
                "error" => "Invalid"
            ]];
        }
    }

    /**
     * Get recent orders
     *
     * @param string $payload Base64 encoded JSON request
     * @return string List of orders
     */
    public function orders($payload)
    {
        $data = json_decode(base64_decode($payload), true);
        if (count($data) == 0) {
            return $data;
        }

        $timestamp = isset($data['request']['timestamp']) ? $data['request']['timestamp'] : '';
        $signature = isset($data['request']['signature']) ? $data['request']['signature'] : '';

        include_once('phpseclib/Crypt/RSA.php');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $rsa_magento = $objectManager->create('Crypt_RSA');
        $rsa_magento->loadKey(self::$public_key);
        if ($rsa_magento->verify($timestamp, base64_decode($signature))) {
            $store_id = isset($data['request']['store_id']) ? $data['request']['store_id'] : '';
            $from = isset($data['request']['from']) ? $data['request']['from'] : '';

            $objectManager->get('Magento\Store\Model\StoreManagerInterface')->setCurrentStore($store_id);
            $store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore($store_id);
            $base_media_url = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

            $filtersStore = [];
            $filtersStore[] = $this->filterBuilder
                ->setField('store_id')
                ->setConditionType('eq')
                ->setValue($store_id)
                ->create();
            $filtersStatus = [];
            $filtersStatus[] = $this->filterBuilder
                ->setField('status')
                ->setConditionType('eq')
                ->setValue('complete')
                ->create();
            $filtersCreatedAt = [];
            $filtersCreatedAt[] = $this->filterBuilder
                ->setField('created_at')
                ->setConditionType('gt')
                ->setValue(date('Y-m-d H:i:s', $from))
                ->create();
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilters($filtersStore)
                ->addFilters($filtersStatus)
                ->addFilters($filtersCreatedAt)
                ->setPageSize(3000)
                ->create();
            $products = $this->orderRepository->getList($searchCriteria);
            $orderCollection = $products->getItems();

            $orders = [];
            foreach ($orderCollection as $order) {
                $order_data = [
                    'order_id' => $order->getId(),
                    'shipping_data' => $order->getShippingAddress()->getData(),
                    'store_id' => $store_id,
                    'store_base_url' => $store->getBaseUrl(),
                    'created_at' => $order->getCreatedAt(),
                    'products' => []
                ];

                $items_count = 0;
                foreach ($order->getAllItems() as $item) {
                    $product = $this->productRepository->getById($item->getProductId());
                    // if not visibility set to "Not visible Individually"
                    if ($product->getVisibility() != 1) {
                        $product_item = [
                            'id' => $item->getProductId(),
                            'product_name' => $product->getName(),
                            'product_url' => $product->getUrlModel()->getUrlInStore($product),
                            'product_image_url' => $base_media_url . 'catalog/product' . $product->getSmallImage(),
                        ];
                        $order_data['products'][] = $product_item;
                        $items_count++;
                    }
                }
                // only if some visible products
                if ($items_count > 0) {
                    $orders[] = $order_data;
                }
            }
            return $orders;
        }
    }
}
