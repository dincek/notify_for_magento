<?php
namespace NotifyApp\Notify\Model;

use Magento\Framework\DataObject\IdentityInterface;
use NotifyApp\Notify\Api\Data\NotifyKeyInterface;

class NotifyKey extends \Magento\Framework\Model\AbstractModel implements NotifyKeyInterface, IdentityInterface
{
    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'notify_key';

    /**
     * @var string
     */
    protected $_cacheTag = 'notify_key';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'notify_key';

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('NotifyApp\Notify\Model\ResourceModel\NotifyKey');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Get Key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->getData(self::KEY);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \NotifyApp\Notify\Api\Data\NotifyKeyInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Set Key
     *
     * @param string $key
     * @return \NotifyApp\Notify\Api\Data\NotifyKeyInterface
     */
    public function setUrlKey($key)
    {
        return $this->setData(self::KEY, $key);
    }
}
