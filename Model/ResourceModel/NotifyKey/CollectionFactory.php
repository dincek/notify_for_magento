<?php
namespace NotifyApp\Notify\Model\ResourceModel\NotifyKey;

/**
 * Factory class for @see NotifyApp\Notify\Model\ResourceModel\NotifyKey\Collection
 *
 * @codeCoverageIgnore
 */
class CollectionFactory implements \NotifyApp\Notify\Model\ResourceModel\NotifyKey\CollectionFactoryInterface
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Instance name to create
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Factory constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        $instanceName = 'NotifyApp\Notify\Model\ResourceModel\\NotifyKey\\Collection'
    ) {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
    }

    /**
     * @inheritdoc
     */
    public function create(array $data = [])
    {
        return $this->_objectManager->create($this->_instanceName, $data);
    }
}
