<?php
namespace NotifyApp\Notify\Model\ResourceModel\NotifyKey;

interface CollectionFactoryInterface
{
    /**
     * Create class instance with specified parameters
     *
     * @param array $data
     * @return \NotifyApp\Notify\Model\ResourceModel\NotifyKey\Collection
     */
    public function create(array $data = []);
}
