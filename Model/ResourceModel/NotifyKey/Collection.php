<?php
namespace NotifyApp\Notify\Model\ResourceModel\NotifyKey;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('NotifyApp\Notify\Model\NotifyKey', 'NotifyApp\Notify\Model\ResourceModel\NotifyKey');
    }
}
