<?php
namespace NotifyApp\Notify\Model\ResourceModel;

class NotifyKey extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('notify_key', 'id');
    }
}
