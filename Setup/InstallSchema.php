<?php

/**
 * Copyright 2016 notifyapp.io. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace NotifyApp\Notify\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use NotifyApp\Notify\Observer\Notify;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var \NotifyApp\Notify\Model\ResourceModel\NotifyKey\CollectionFactory
     */
    private $notifyKeyCollectionFactory;

    public function __construct(
        \NotifyApp\Notify\Model\ResourceModel\NotifyKey\CollectionFactory $notifyKeyCollectionFactory
    ) {
        $this->notifyKeyCollectionFactory = $notifyKeyCollectionFactory;
    }

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $table = $setup->getConnection()
            ->newTable(
                $setup->getTable('notify_key')
            )
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'key',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['identity' => false, 'nullable' => false, 'primary' => false],
                'Key'
            );
        $setup->getConnection()->createTable($table);

        $data = [
            'id' => 1,
            'key' => hash('sha256', uniqid() . time()),
        ];
        $notifyKeys = $this->notifyKeyCollectionFactory
            ->create()
            ->addFilter('id', 1);
        $key = '';
        foreach ($notifyKeys as $obj) {
            $key = $obj->getKey();
        }
        if ($key == '') {
            $setup->getConnection()->insert($setup->getTable('notify_key'), $data);
        }

        // call init to notify server
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        foreach ($storeManager->getStores() as $store) {
            Notify::notifyInit($store->getBaseUrl());
        }

        $setup->endSetup();
    }
}
