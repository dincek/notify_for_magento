<?php

/**
 * Copyright 2016 notifyapp.io. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace NotifyApp\Notify\Setup;

use NotifyApp\Notify\Observer\Notify;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UninstallInterface;

class Uninstall implements UninstallInterface
{
    /**
     * @var \NotifyApp\Notify\Model\ResourceModel\NotifyKey\CollectionFactory
     */
    private $notifyKeyCollectionFactory;

    public function __construct(
        \NotifyApp\Notify\Model\ResourceModel\NotifyKey\CollectionFactory $notifyKeyCollectionFactory
    ) {
        $this->notifyKeyCollectionFactory = $notifyKeyCollectionFactory;
    }

    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $notifyKeys = $this->notifyKeyCollectionFactory
            ->create()
            ->addFilter('id', 1);
        $key = '';
        foreach ($notifyKeys as $obj) {
            $key = $obj->getKey();
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        foreach ($storeManager->getStores() as $store) {
            Notify::notifyUninstall($store->getBaseUrl(), $key);
        }

        $setup->getConnection()->dropTable($setup->getTable('notify_key'));

        $setup->endSetup();
    }
}
