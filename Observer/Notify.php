<?php

/**
 * Copyright 2016 notifyapp.io. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace NotifyApp\Notify\Observer;

use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Notify implements ObserverInterface
{
    public static $DOMAIN = "https://notifyapp.io";

    public static $VERSION = "1.0.0";

    private $productRepository;

    /**
     * @var \NotifyApp\Notify\Model\ResourceModel\NotifyKey\CollectionFactory
     */
    private $notifyKeyCollectionFactory;

    public function __construct(
        ProductRepository $productRepository,
        \NotifyApp\Notify\Model\ResourceModel\NotifyKey\CollectionFactory $notifyKeyCollectionFactory
    ) {
        $this->productRepository = $productRepository;
        $this->notifyKeyCollectionFactory = $notifyKeyCollectionFactory;
    }

    public function execute(Observer $observer)
    {
        $notifyKeys = $this->notifyKeyCollectionFactory
            ->create()
            ->addFilter('id', 1);
        $key = '';
        foreach ($notifyKeys as $obj) {
            $key = $obj->getKey();
        }

        $order = $observer->getEvent()->getOrder();
        $order_id = $order->getIncrementId();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
        $objectManager->get('Magento\Store\Model\StoreManagerInterface')->setCurrentStore($store->getId());
        $base_media_url = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        $order_data = [
            'order_id' => $order_id,
            'shipping_data' => $order->getShippingAddress()->getData(),
            'store_id' => $store->getId(),
            'store_base_url' => $store->getBaseUrl(),
            'created_at' => $order->getCreatedAt(),
            'products' => []
        ];

        foreach ($order->getAllItems() as $item) {
            $product = $this->productRepository->getById($item->getProductId());
            // if not visibility set to "Not visible Individually"
            if ($product->getVisibility() != 1) {
                $product_item = [
                    'id' => $item->getProductId(),
                    'product_name' => $product->getName(),
                    'product_url' => $product->getUrlModel()->getUrlInStore($product),
                    'product_image_url' => $base_media_url . 'catalog/product' . $product->getSmallImage(),
                ];
                $order_data['products'][] = $product_item;
            }
        }

        // only if some visible products
        if (count($order_data['products']) > 0) {
            $data = json_encode($order_data);
            $time = time();
            $ch = curl_init(self::$DOMAIN . '/magento_webhook?url=' . urlencode($order_data['store_base_url']) .
                '&timestamp=' . $time . '&hmac=' . hash_hmac('sha256', $data, $key));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data)]);

            $result = curl_exec($ch);

            if (!curl_errno($ch)) {
                switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                    case 200:  # OK
                        break;
                    case 401:
                        // reinitialize key exchange
                        self::notifyInit($order_data['store_base_url']);
                        break;
                    default:
                        // Unexpected HTTP code: $http_code
                        break;
                }
            }
        }
    }

    public static function notifyInit($shop_url)
    {
        $ch2 = curl_init(self::$DOMAIN . '/magento_init?url=' . urlencode($shop_url) . '&timestamp=' . time() .
            "&version=" . self::$VERSION);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch2);
    }

    public static function notifyUninstall($shop_url, $key)
    {
        $time = time();
        $ch2 = curl_init(self::$DOMAIN . '/magento_uninstall?url=' . urlencode($shop_url) . '&timestamp=' . $time .
            '&hmac=' . hash_hmac('sha256', $shop_url.$time, $key));
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch2);
    }

    public static function notifyCheck($shop_url, $key)
    {
        $time = time();
        $ch2 = curl_init(self::$DOMAIN . '/magento_check?url=' . urlencode($shop_url) . '&timestamp=' . $time .
            '&hmac=' . hash_hmac('sha256', $shop_url.$time, $key));
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch2);
        return json_decode($result, true);
    }
}
